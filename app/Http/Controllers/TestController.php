<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TestController extends Controller
{
    public function index()
    {

        $a=DB::table('t1')->get();
        dd($a);
    }

    public function insert()
    {
        $a=DB::table('t1')->insert([
            'name' => 'test',
        ]);
        dd($a);
    }

    public function update($id)
    {
        $a=DB::table('t1')->where('id',$id)->update(['name'=>'update']);
        dd($a);
    }

}
